# Eucaryotes

Projet SQL d'une base de données de gestion de plantes en accord avec des standards de taxonomie.

https://fr.wikipedia.org/wiki/Taxonomie :

La taxonomie ou taxinomie est une branche des sciences naturelles qui a pour objet l'étude de la diversité du monde vivant. Cette activité consiste à décrire et circonscrire en termes d'espèces les organismes vivants et à les organiser en catégories hiérarchisées appelées taxons. 


Le point d'entrée de ce projet est une table `taxonomie` présentant un champ `parent` comme référence vers un autre taxon (foreign key sur la meme table).
L'objectif principal est d'utiliser et d'optimiser le langage SQL (sous MySQL) à travers des vues et des procédures stockées.


```sh
Eucaryotes/
            ...eucaryotes.sql
            ...populate.sql
            ...view.sql
            ...stored_process.sql
```
Utilisation :

Les scripts `eucaryotes.sql` et `table.sql` instancient la base de donnée ainsi que les différentes tables.

On utilise `populate.sql` pour avoir quelques données.

Les autres scripts (`view.sql`, `stored_process.sql`...) chargent le reste du projet.


## Docker 
<i>Fonctionne sous Ubuntu 20.04.5 LTS, Docker 20.10.23</i>

```shell
cd docker
docker compose up
```

`Pensez à vérifier les volumes montés pour la persistence des données, pas sur que ce soit correct en l'état`

```yaml
    volumes:
      - /opt/mysql/eucaryotes:/var/lib/mysql
      - ../sql_script/eucaryotes.sql:/docker-entrypoint-initdb.d/eucaryotes.sql:ro
```

- [ ] Un container `mysql`
- [ ] Un container `phpmyadmin`
- [ ] Deux utilisateurs : `root` et `eucaryotes_user`, avec mot de passe en variables d'environnement
- [ ] Une base de données **Eucaryotes** (`eucaryotes_user` a des droits)

## Todo :  </i>

 - [ ] `assert.sql` ? (comment? diff ? cat ?)
 - [ ] Consulter tous les taxons référencés (`view.sql`)
 - [ ] Renseigner un nouveau taxon (depuis une liste de parents finie)
 - [ ] Consulter un taxon et son parent (`stored_process.sql`)
 - [ ] Consulter uniquement les cultivars (trigger, si plus de X enfants alors "arbre" devient une "famille" et plus seulement un taxon ordinaire => plus tard)
 - [ ] Consulter la classification complete d'un taxon
 - [ ] Consulter l'ensemble des taxons à N degrés
 - [ ] Ajouter les plantations d'un cultivar, les jardins, les utilisateurs
 - [ ] Ajouter les animaux (parasites)
 - [ ] AJouter tables pivot (plante x deteste parasite y mais plante z detruit parasite y  cf absinthe pucerons rosiers => plus tard mais cool)
 - [ ] Pour une zone donnée dans un jardin, quelles plantes ne surtout pas planter 
 - [ ] Pour une zone donnée dans un jardin, quelles plantes à planter
 - [ ] Déplacer une branche 
 - [ ] Ajouter un taxon au milieu d'une branche
 - [ ] Comment on traite l'output ? (regne-division-classe-ordre-famille-genre)

```mermaid
flowchart TD

    Eucaryotes --> Plantae & Fungi & Animalia
    Plantae --> Parent1 & Parent2
    Parent1 --> Enfant1:::p & Enfant2 & Enfant3
    Parent2 --> Enfant4
    Enfant1 --> Plante2:::p
    Enfant1 --> Plante3:::p

    classDef p fill:#FF9A00
```