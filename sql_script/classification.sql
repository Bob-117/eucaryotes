-- DROP PROCEDURE classification_with_output;

DELIMITER |

CREATE PROCEDURE IF NOT EXISTS `Eucaryotes`.classification_with_output(IN taxon_id INT, OUT final_path TEXT)
BEGIN
    WITH RECURSIVE recursive_concat AS (
        SELECT 1 as n, taxon, parent_id, CAST(taxon AS CHAR(1000)) AS path
        FROM taxonomie
        WHERE id = taxon_id
        UNION ALL
        SELECT recursive_concat.n + 1 AS n, t.taxon, t.parent_id, CONCAT(recursive_concat.path, '-', t.taxon)
        FROM taxonomie t
        JOIN recursive_concat ON t.id = recursive_concat.parent_id
    )
    SELECT n, path
    FROM recursive_concat
    ORDER BY n DESC
    LIMIT 1;
END |
DELIMITER ;

-- CALL classification_with_output(122, @final);
-- SELECT @final;