INSERT INTO `Eucaryotes`.`taxonomie` (id, taxon, parent_id) VALUES (-1, 'Eucaryotes', Null);
INSERT INTO `Eucaryotes`.`taxonomie` (id, taxon, parent_id) VALUES (1, 'Plantae', -1), (2, 'Fungi', -1), (3, 'Animalia', -1); 
INSERT INTO `Eucaryotes`.`taxonomie` (id, taxon, parent_id) VALUES
/* TOMATE */
(100, 'Tracheobionta', 1), -- Sous regne
(101, 'Magnoliophyta', 100), -- Division
(102, 'Magnoliopsida', 101), -- Classe
(103, 'Asteridae', 102), -- Sous classe
(104, 'Solanales', 103), -- Ordre
(105, 'Solanaceae', 104), -- Famille
(106, 'Solanum', 105), -- Genre
(107, 'Solanum lycopersicum', 106), -- Espece
/* LISERON */
(110, 'Convolvulaceae', 104), -- Famille
(111, 'Convolvulus', 110), -- Genre
(112, 'Convolvulus lanuginosus', 111), -- Espece liseron laineux protégé en PACA
(113, 'Convolvulus tricolor', 111), -- Espece liseron belle de jour
/* PATATE DOUCE */
(120, 'Ipomoeeae', 110), -- Tribu
(121, 'Ipomoea', 120), -- Genre
(122, 'Ipomoea batatas', 121), -- Espece
/* BLE */
(130, 'Liliopsida', 101), -- Classe
(131, 'Commelinidae', 130), -- Sous classe
(132, 'Cyperales', 131), -- Ordre
(133, 'Poaceae', 132), -- Famille
(134, 'Pooideae', 133), -- Sous famille
(135, 'Triticeae', 134), -- Tribu
(136, 'Triticum', 135), -- Genre
(137, 'Triticum turgidum', 136), -- Espece BLE DUR
(138, 'Triticum turgidum subsp. durum', 137), -- Sous espece
(139, 'Triticum aestivum', 136); -- Espece BLE TENDRE/FROMENT 	

/* INFORMATIONS TAXON */
INSERT INTO `Eucaryotes`.`informations_taxon` (contenu, taxon_id) VALUES
('Regne des plantes', 1),
('Regne des champignons', 2),
('Regne des animaux', 3),
('Espece liseron laineux protégé en PACA', 112), 
('Espece liseron belle de jour', 113),
('Du blé dur', 137),
('Du blé tendre / froment', 139),
('La patate douce', 122);
