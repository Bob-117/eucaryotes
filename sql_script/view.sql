USE `Eucaryotes`;

CREATE OR REPLACE VIEW tous_les_taxons AS
SELECT t.id as "Index", t.taxon as "Nom", i.contenu as "Informations"
FROM `Eucaryotes`.`taxonomie` t
LEFT JOIN `Eucaryotes`.`informations_taxon` i ON t.id = i.taxon_id
ORDER BY ISNULL(i.contenu), i.contenu ASC;

CREATE OR REPLACE VIEW tous_les_taxons_sans_index AS
SELECT a.Nom, a.Informations FROM tous_les_taxons as a;
